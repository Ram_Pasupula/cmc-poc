import logging
from datetime import datetime
from pytz import timezone
import boto3
import io
import pandas as pd
import json
import sys
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
date = datetime.now(timezone('US/Eastern'))
date = date.strftime('%Y-%m-%d')
#endpointUrl = os.environ['ENDPOINT_URL']
#Document Endpoint:	doc-cmc-search-bl2bfbfr752ryxfhtbtyhgzqk4.us-east-1.cloudsearch.amazonaws.com
endpointUrl = "http://doc-cmc-search-bl2bfbfr752ryxfhtbtyhgzqk4.us-east-1.cloudsearch.amazonaws.com"
class CMCCloudSearchUploadService(object):
    def __init__(self):
        print("CMCCloudSearchService initialised")
       
    def upload(self, jsonData):
        log.info('CloudSearch  doc upload ::   ' )
        
        try:
            response = self.__uploadDoc(jsonData)
        except Exception as ex:
            log.error(f"failed to process the request with {ex}") 
            raise ex
        else: 
            return  response   

    
    def __uploadDoc(self,jsonData):
        try:
            client = boto3.client('cloudsearchdomain',endpoint_url= endpointUrl)
            
            #doclist = json.dumps([jsonData])
            print(client.upload_documents(documents=jsonData, contentType="application/json"))
            
        except Exception as ex:
            log.error(f"failed to process the request with {ex}")  
            raise ex 
        else:       
            return "uploaded doc "
     
if __name__ == '__main__':
   print(" ")
   
