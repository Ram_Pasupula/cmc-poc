import logging,sys

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)

class CMCDBTransTimeoutException(Exception):
    pass
class CMCDBTransFailedException(Exception):
    pass
class CMCException(object):
    def __init__(self):
        pass
    @classmethod
    def log(clc):
        exc_type, exc_obj, tb = sys.exc_info()
        lineno = tb.tb_lineno
        filename = tb.tb_frame.f_code.co_filename
        ex_msg:str = filename+":"+str(lineno)+"-"+str(exc_obj)
        log.error(ex_msg)