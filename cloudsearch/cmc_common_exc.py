import sys

class CMCDBTimeoutException(Exception):
    pass
class CMCDBFailedException(Exception):
    pass
class CMCExceptionUtil(object):
    def __init__(self):
        pass
    @classmethod
    def log(clc,logger):
        exc_type, exc_obj, tb = sys.exc_info()
        lineno = tb.tb_lineno
        filename = tb.tb_frame.f_code.co_filename
        ex_msg:str = filename+":"+str(lineno)+"-"+str(exc_obj)
        logger.error(ex_msg)