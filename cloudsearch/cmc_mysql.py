import logging
from sqlalchemy import create_engine
import pandas as pd
import numpy as np
import boto3
import json
from cmc_secret import cmc_secret as secret
log = logging.getLogger(__name__)
chunksize = 500
class cmc_mysql_conn:   
        
    def getConn(sectretDict):
        log.info("get MySQL connection")
        try:
            print(sectretDict['host'])
            engine = create_engine('mysql+pydataapi://',
                    connect_args={
                        'resource_arn': sectretDict['db_clust_arn'],
                        'secret_arn': sectretDict['db_secret_arn'],
                        'database': "CMCDB"
                    }, echo=False
                )
        except BaseException as error:
            log.error("Unknown error while mysql get connection: " + error.response['Error']['Message'])
        else:
            return engine
       
    if __name__ == '__main__':
        sectretDict = json.loads(secret.get_secret("qa"))
        
        engine = getConn(sectretDict) 
        print(engine )     
        df_status = pd.read_sql("show table status like 'video';", con = engine)
        rownum = df_status.loc[0, "Rows"]
        print(f'rownum: {rownum}')
        print(f'chunksize: {chunksize}')
        space = np.arange(0, rownum, chunksize)
        space = space.tolist()
        space.append(rownum)
        df_list = []
        for i in space:
            df = pd.read_sql("select * from video LIMIT {}, {}".format(i, chunksize), con = engine)
            df_list.append(df)
            big_df = pd.concat(df_list)
        print(big_df)   
       #cluster_arn = "arn:aws:rds:us-east-1:970547971489:cluster:cmc-prod-db"
       #secret_arn = "arn:aws:secretsmanager:us-east-1:970547971489:secret:cmc-analytics-creds-prod-1Jguqc"
       #with aurora_data_api.connect(aurora_cluster_arn=cluster_arn, secret_arn=secret_arn, database="CMCDB") as conn:
        #  with conn.cursor() as cursor:
        #    cursor.execute("select * from video")
        #    data = cursor.fetchall()