import logging
from datetime import datetime
from pytz import timezone
import boto3
import io
import pandas as pd
import json
import hashlib
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
date = datetime.now(timezone('US/Eastern'))
date = date.strftime('%Y-%m-%d')
#endpointUrl = os.environ['ENDPOINT_URL']
#Document Endpoint:	doc-cmc-search-bl2bfbfr752ryxfhtbtyhgzqk4.us-east-1.cloudsearch.amazonaws.com
endpointUrl = "http://search-cmc-search-bl2bfbfr752ryxfhtbtyhgzqk4.us-east-1.cloudsearch.amazonaws.com"
class CMCCloudSearchService(object):
    def __init__(self):
        print("CMCCloudSearchService initialised")
       
    def search(self, query,size,start):
        log.info(f'CloudSearch  ::  {query} ' )
        
        try:
            response = self.__searchDoc(query,size,start)
        except Exception as ex:
            log.error(f"failed to process the request with {ex}") 
        else: 
            return  response   

    
    def __searchDoc(self,query,sizeT,start):
        try:
            client = boto3.client('cloudsearchdomain',endpoint_url= endpointUrl)
            response = client.search(
                    query = query, # your search string
                    size = sizeT,
                    cursor='initial',
                    #start =10
                    #cursor='VecIqMEQQW9JSVFFQTlEeVF5TmpnMh4',
                    queryParser='structured'
                    ,sort='id desc',
                    returnFields ='id'

            )
        except Exception as ex:
            log.error(f"failed to process the request with {ex}")   
        else:       
            return response
     
if __name__ == '__main__':
   print(" ")
   
